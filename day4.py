def make_board(board_str):
    board = []
    i = 0
    for line in board_str.split("\n"):
        board.append([])
        for item in line.split():
            board[i].append([int(item), False])
        i += 1
    return (board)

def update_board(board, num):
    for i in range(0, len(board)):
        for j in range(0, len(board[i])):
            if board[i][j][0] == num:
                board[i][j][1] = True
    return (board)

def check_vertical(board):
    for i in range(0, len(board)):
        marked_true = 0
        for j in range(0, len(board[i])):
            if board[j][i][1] == True:
                marked_true += 1
        if marked_true == len(board[i]):
            return (True)
    return (False)

def check_horizontal(board):
    for row in board:
        marked_true = 0
        for item in row:
            if item[1] == True:
                marked_true += 1
        if marked_true == len(row):
            return (True)
    return (False)
                


def check_bingo(board):
    return (check_vertical(board) or check_horizontal(board))
            

def get_score(board, num):
    result = 0
    for row in board:
        for item in row:
            if item[1] == False:
                result += item[0]
    return (result * num)


def main():
    with open("inputs/day4") as f:
        lines = f.readlines()
    seq = [int(x) for x in lines[0].split(',')]
    str_input = ""
    for l in lines[2:]:
        str_input += l
    boards = [make_board(b.strip()) for b in str_input.split("\n\n")]
    winners = []
    for n in seq:
        for i in range(0, len(boards)):
            boards[i] = update_board(boards[i], n)
            if check_bingo(boards[i]) and boards[i] not in winners:
                winners.append(boards[i])
                print(i, n, get_score(boards[i], n))

if __name__ == "__main__":
    main()