def main():
    d = {
        0: 0,
        1: -1,
        2: -1,
        3: -1,
        4: -1,
        5: -1,
        6: -1,
        7: -1,
        8: -1
    }
    with open("testinputs/day6") as f:
        fish = f.readlines()[0].split(",")
    for f in fish:
        if (d[int(f)]) == -1:
            d[int(f)] += 1
        d[int(f)] += 1
    day = 0
    while day < 100:
        for k in d.keys():
            fish_amount = d[k]
            if k == 0:
                d[8] += fish_amount
                d[k] -= fish_amount
                d[6] += fish_amount
            else:
                d[k - 1] += fish_amount

if __name__=="__main__":
    main()
