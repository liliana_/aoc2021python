def get_bits(lines):
    bits = []
    for i in range(0, len(lines[0]) - 1):
       bits.append([])
    for l in lines:
        for i in range(0, len(lines[0]) - 1):
            bits[i].append(l[i])
    return (bits)

def get_ox_contenders(lines, commonbit, index):
    contenders = []
    for l in lines:
        if l[index] == commonbit:
            contenders.append(l)
    return (contenders)
        
def get_co_contenders(lines, commonbit, index):
    contenders = []
    for l in lines:
        if l[index] != commonbit:
            contenders.append(l)
    return (contenders)

def get_ox_commonbits(lines):
    bits = get_bits(lines)
    ox_commonbits = []
    for l in bits:
        if l.count('1') >= l.count('0'):
            ox_commonbits.append('1')
        else:
            ox_commonbits.append('0')
    return (ox_commonbits)

def get_co_commonbits(lines):
    bits = get_bits(lines)
    co_commonbits = []
    for l in bits:
        if l.count('1') >= l.count('0'):
            co_commonbits.append('1')
        else:
            co_commonbits.append('0')
    return (co_commonbits)

def main():
    with open("testinputs/day3") as f:
        lines = f.readlines()

    i = 0
    ox = lines
    co2 = lines
    while len(ox) != 1:
        ox_commonbits = get_ox_commonbits(ox)
        ox = get_ox_contenders(ox, ox_commonbits[i], i)
        i += 1
    i = 0
    while len(co2) != 1:
        co_commonbits = get_co_commonbits(co2)
        co2 = get_co_contenders(co2, co_commonbits[i], i)
        i += 1
    print(int(ox[0], 2) * int(co2[0], 2))
    
if __name__ == "__main__":
    main()
