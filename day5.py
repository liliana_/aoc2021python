from collections import defaultdict

class Line():
    def __init__(self, startstr, endstr):
        start = [int(x) for x in startstr.split(',')]
        end = [int(x) for x in endstr.split(',')]
        
        self.x1 = start[0]
        self.y1 = start[1]
        self.x2 = end[0]
        self.y2 = end[1]

def make_line(str):
    str_split = str.split(" -> ")
    line = Line(str_split[0], str_split[1])
    return (line)

def draw_line(line, d):
    cur_x = line.x1
    cur_y = line.y1
    while cur_x != line.x2 or cur_y != line.y2:
        d[(cur_x, cur_y)] += 1
        if cur_x < line.x2:
            cur_x += 1
        elif cur_x > line.x2:
            cur_x -= 1
        if cur_y < line.y2:
            cur_y += 1
        elif cur_y > line.y2:
            cur_y -= 1
    d[(cur_x, cur_y)] += 1
    return (d)

def main():
    with open("inputs/day5") as f:
        fl = f.readlines()
    lines = [make_line(x.strip()) for x in fl]
    d = defaultdict(lambda: 0)
    overlaps = 0
    for line in lines:
        d = draw_line(line, d)
    for v in d.values():
        if v > 1:
            overlaps += 1
    print(overlaps)

if __name__ == "__main__":
    main()