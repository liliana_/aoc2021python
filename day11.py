g_flashers = []

def int_list(line):
    lst = []
    for c in line:
        lst.append(int(c))
    return (lst)

def is_valid(x, y):
    if x > 9 or x < 0:
        return (False)
    elif y > 9 or y < 0:
        return (False)
    return (True)

def flash(input, x, y):
    for i in range(-1, 2):
        for j in range(-1, 2):
            if is_valid(x + i, y + j):
                input[x + i][y + j] += 1
    for i in range(-1, 2):
        for j in range(-1, 2):
            if is_valid(x + i, y + j):
                if input[x + i][y + j] > 9 and (x + i, y + j) not in g_flashers:
                    g_flashers.append((x + i, y + j))
                    input = flash(input, x + i, y + j)
    return (input)

def all_zeroes(input):
    zero_lines = 0
    for line in input:
        if line.count(0) == 10:
            zero_lines += 1
    return zero_lines == 10


def main():
    with open("inputs/day11") as f:
        input = [int_list(line.strip()) for line in f.readlines()]
    limit = 0
    flashcount = 0
    global g_flashers
    while True:
        g_flashers = []
        for x in range(0, len(input)):
            for y in range(0, len(input)):
                input[x][y] += 1
        for x in range(0, len(input)):
            for y in range(0, len(input)):
                if input[x][y] > 9 and (x, y) not in g_flashers:
                    g_flashers.append((x, y))
                    input = flash(input, x, y)   
        for x in range(0, len(input)):
            for y in range(0, len(input)):
                if input[x][y] > 9:
                    input[x][y] = 0
                    flashcount += 1
        if limit == 99:
            print("part 1:", flashcount)
        if all_zeroes(input):
            print("part 2:", limit + 1)
            break
        limit += 1

if __name__ == "__main__":
    main()